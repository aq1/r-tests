# Lists (~ python dictionnary)

l <- list(a="asdf", b=1, c=T)
l
l$a

ll <- vector(mode="list")
ll
ll["g"] <- "h"
ll[["i"]] <- "j"
ll$i
ll[1]
ll["h"]
ll["i"]
ll[[1]]
class(ll)
class(ll[1])
class(ll[[1]])
ll$i <- NULL
c( list(a=1, b=2), list(c=3))
length(ll)
names(ll)
unlist(ll)
lapply(list(1:4, 30:32), median)
sapply(list(1:4, 30:32), median)
c(list(a = 1, b = 2, c = list( d = 3, e = 4)))
c(list(a = 1, b = 2, c = list( d = 3, e = 4)), recursive = T)

# Extracting word locations from a text

findwords <- function(text_file){
  raw_words = scan(text_file, "")
  words = list()
  for (i in 1:length(raw_words)) {
    word <- raw_words[i]
    words[[word]] <- c(words[[word]], i)
  }
  return(words)
}
findwords("example.txt")

words_with_order <- function(words_list) { # order the results
  sorted_names = sort(names(words_list))
  return(words_list[sorted_names])
}

ws <- words_with_order(findwords("example.txt"))

barplot(sapply(ws, length))
