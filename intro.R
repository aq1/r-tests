mean(abs(rnorm(100)))

rnorm(10)

source("intro.r")

pdf("xh.pdf")
hist(rnorm(100))
dev.off()

hist(Nile)

# functions
oddcount <- function(x) {
  k <- 0
  for (n in x) {
    if (n %% 2 == 1) k <- k+1
  }
  return(k)
}
oddcount(c(1,2,3))

38%%7

f <- function(x) return(x+y)
y <- 2
f(5)

x <- 8

x <- c(5,12,13)
length(x)
mode(x)

y <- "abc"
mode(y)
length(y)
u <- paste("abc", "de", "f")
v <- strsplit(u, " ")
v

# Matrices
m <- rbind(c(1,4), c(2,2))
m
m[1,2]
m%*%m
m[1,]
m[,2]

# Lists
x <- list(u=2, v="abc")
x
x$u
x$v
hn <-hist(Nile)
hn
hn$breaks
str(hn) # Outputs a structure

# Data frames
d <- data.frame(list(kids=c("Jack", "Jill"), ages=c(12, 10)))
d

# Classes
summary(hn)
plot(hn)

# Regression analysis of exam grades
examsquiz <- read.table("ExamsQuiz.txt", header=TRUE)
examsquiz
lma <- lm(examsquiz[,2] ~ examsquiz[,1])# predict Final from Midterm
lma
attributes(lma)
str(lma)
lma$coefficients
lma
summary(lma)
lmb <- lm(examsquiz$FINAL ~ examsquiz$MIDTERM + examsquiz$AVG_QUIZ)
summary(lmb)

# Examples
example(seq)
example("persp")
help.search("multivariate normal")
?mvrnorm
help(package=MASS)
?files

# Packages
library(MASS)
path.package()
install.packages("mvtnorm", "/home/aqu/Documents/r-tests")
.libPaths("mvtnorm")
.libPaths("/home/aqu/Documents/r-tests")
.libPaths()
example(".libPaths")
library()
search()
library(mvtnorm)
detach(package:mvtnorm)
