# Vectors

x <- c(32, 34 , 421, 3)
x <- c(x[1:3], 11234, x[4])
x
length(x)

first1 <- function(x) {
  for(i in 1:length(x)) {
    if (x[i] == 1) break
  }
  i
}
first1(x)
y <- c(2,3,1,4)
first1(y)

m <- rbind(c(1,2), c(3,4))
m
m + c(10,11,12,13)

y <- vector(length = 2)
y
y[1] <- 3
y[2] <- 5

c(1,2,3,4) + c(5,6,7,8,9,10)
rbind(c(1,2), c(3,4))
rbind(c(1,2), c(3,4)) + c(1,2)

"+"(1,4)

x <- c(1,2,3)
x*c(4,0,1)

y <- c(1.1, 1.2, 1.4, 1.5)
y[c(1,4,6)]
y[2:8]
range <- 3:4
y[range]
y[c(1,1,1,4,6)]
y[-1:-2]
y[-length(y)]

5:9

seq(from=12, to=20, by=2)

rep(c(12,13), times=4)

any(c(FALSE, FALSE))
all(c(TRUE, TRUE))

# Runs of 1
findruns <- function(x,k){
  n <- length(x)
  runs <- NULL
  for (i in 1:(n-k+1)) {
    if (all(x[i:(i+k-1)]==1)) runs <- c(runs, i)
  }
  runs
}
v <- c(1,0,0,1,1,1,0,1,1)
findruns(v,2)
findruns(v,3)

# insert check of vector size
if (length(c) != 1) stop("vector c not allowed")

# sapply to produce matrices
z12 <- function(z) return(c(z, z^2))
sapply(1:8, z12)

# Filtering
x = 1:10
x[x>4] <- 0
x
y = c(x, NA)
y
subset(y, y<2)
y[y<2]
which(y>3)

y <- ifelse((1:10)%%2==0,1,0)
y

a <- 1:3
a
a[-1]
a[-3]
help(lapply)
list(1:3, 4:6)
c(1,0,-1) == c(-1, 1, -1)
mean(c(1,0,-1) == c(-1, 1, -1))
diff(c(1:4, 10, 12))
sign(diff(c(1:4, 10, 12, 10, 10)))

# Abalones
aba <- read.csv("Abalone.data", header = F, as.is = T)
summary(aba)
grps <- list()
pchvec <- ifelse(aba$V1 == "M", "o", "x")
pchvec
help(plot)
plot(aba%V2, ava$V3, pch=pchvec)
